#!/bin/bash

set eux -o pipefail

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	if [ "$(grep -Ei 'debian|buntu|mint' /etc/*release)" ]; then
		echo ""
		echo "Installing Python 3..."
		add-apt-repository ppa:deadsnakes/ppa
		apt update
		apt install python3.8
		rm /usr/bin/python
		ln -s $(which python3.8) /usr/bin/python
		echo ""
		echo "Installing Ansible..."
		apt install software-properties-common
		apt-add-repository --yes ppa:ansible/ansible 
		apt update
		apt install ansible
	else
		echo "Linux flavour not supported"
	fi
elif [[ "$OSTYPE" == "darwin"* ]]; then
	echo ""
	echo "Installing Ansible..."
	curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
	python get-pip.py
	pip install ansible
else
	echo "OS not supported"
fi
