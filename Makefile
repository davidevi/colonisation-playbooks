
req:
	@sudo ./scripts/prerequisites.bash

dev-station:
	@sudo -H ansible-playbook ansible/development-workstation.yaml

dev-server:
	@sudo -H ansible-playbook ansible/development-server.yaml

home-server:
	ansible-playbook ansible/home-server.yaml
